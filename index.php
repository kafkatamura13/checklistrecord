<!DOCTYPE html>
<html>
<head>
	<title>Log In</title>	
	<link rel="icon" type="image/png" href="img/767175.png">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<style type="text/css">
		body {
		font-family: Arial , Helvetica , sans-serif;
		width: 300px;
		background: #fff;
		border:4px solid #000;
		margin:20px auto;
		text-align: center;
		border-radius: 10px;
		height: 400px;
		}
		h1 {
		padding: 10px;
		font-size: 14px;
		background: #000;
		margin:0;
		color:#fff;
		}
		form {
			margin-top:20px;
		}
		input[type=text],
		input[type=password],
		textarea,
		select {
		width: 200px;
		}
		input[type=submit]{
		margin-top:30px;

		}
		.footer h1 {			
			border-top: 4px solid #feca01;
			height: 35px;
		}
		.footer p {
			font-weight: bold;
			text-transform: uppercase;
			font-size: 14px;
		}

	</style>

</head>
<body> <?php 
 	$state=$_REQUEST['state'];
 	$errMsg="Wrong user name or Password! Please Try Again ";
 	$success="Registration Successful!Please Login to Continue";
 	?>


  <div class="header">
  	<img src="img/qualy.svg"/><br>

  </div>
 

 <form action="login.php"k method="post">
 	<label for="name">Name</label>
 	<input type="text" name="name" id="name" required>

 	<label for="password">Password</label>
 	<input type="password" name="password" id="password" required>

 	<br><br>
 	<input type="submit" value="Admin Login" >
 	<div class="footer">
 	<p>Login to Team QA</p><br>
 	<h1><img src="img/qualy.svg"/></h1><br>
 	</div>
 </form>
  	<?php
 	if($state==1){
 		echo "<script type='text/javascript'>alert('$errMsg');</script>";
 	}
 	elseif ($state==2) {
 		echo "<script type='text/javascript'>alert('$success');</script>";
 	}
  ?>
</body>
</html>