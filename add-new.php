<?php 
	require 'confs/auth.php';
	session_start();
 ?>
	<!DOCTYPE html>
	<html>
	<head>
		<title></title>
		<link rel="icon" type="image/png" href="img/767175.png">
		<link rel="stylesheet" type="text/css" href="css/style.css"/>

		<style type="text/css">

		input[type=text],		
		input[type=password],
		textarea,
		select {
		font-family: Arial, Helvetica, sans-serif;
		border:1px solid #ddd;
		padding:6px;
		width: 150px;
		background:#fff;
		border-radius: 4px;
	}
		.form-style-5{
	    max-width: 500px;
	    padding: 10px 20px;
	    background: #f4f7f8;
	    margin: 10px auto;
	    padding: 20px;
	    background: #f4f7f8;
	    border-radius: 8px;
	    font-family: Georgia, "Times New Roman", Times, serif;
	}
	.form-style-5 fieldset{
	    border: none;
	}
	.form-style-5 legend {
	    font-size: 1.4em;
	    margin-bottom: 10px;
	}
	.form-style-5 label {
	    display: block;
	    margin-bottom: 8px;
	}
	.form-style-5 input[type="text"],
	.form-style-5 input[type="date"],
	.form-style-5 input[type="datetime"],
	.form-style-5 input[type="email"],
	.form-style-5 input[type="number"],
	.form-style-5 input[type="search"],
	.form-style-5 input[type="time"],
	.form-style-5 input[type="url"],
	.form-style-5 textarea,
	.form-style-5 select {
	    font-family: Georgia, "Times New Roman", Times, serif;
	    background: rgba(255,255,255,.1);
	    border: none;
	    border-radius: 4px;
	    font-size: 16px;
	    margin: 0;
	    outline: 0;
	    padding: 7px;
	    width: 100%;
	    box-sizing: border-box; 
	    -webkit-box-sizing: border-box;
	    -moz-box-sizing: border-box; 
	    background-color: #e8eeef;
	    color:#8a97a0;
	    -webkit-box-shadow: 0 1px 0 rgba(0,0,0,0.03) inset;
	    box-shadow: 0 1px 0 rgba(0,0,0,0.03) inset;
	    margin-bottom: 30px;
	    
	}
		.form-style-5 input[type="text"]:focus,
		.form-style-5 input[type="date"]:focus,
		.form-style-5 input[type="datetime"]:focus,
		.form-style-5 input[type="email"]:focus,
		.form-style-5 input[type="number"]:focus,
		.form-style-5 input[type="search"]:focus,
		.form-style-5 input[type="time"]:focus,
		.form-style-5 input[type="url"]:focus,
		.form-style-5 textarea:focus,
		.form-style-5 select:focus{
		    background: #d2d9dd;
		}
		.form-style-5 select{
		    -webkit-appearance: menulist-button;
		    height:35px;
		}
		.form-style-5 .number {		   
		    color: #000;
		    float: right;	
		    font-family: impact;
		    font-weight: bold;
		    margin-left: 100px;	   
		    display: inline-block;
		    font-size: 24px;		 
		    text-align: right;
		  
		}

		.form-style-5 input[type="submit"],
		.form-style-5 input[type="button"]
		{
		    position: relative;
		    display: block;
		    padding: 2px 20px;
		    color: #FFF;
		    margin: 0 auto;
		    background: #feca01;
		    font-size: 18px;
		    text-align: center;
		    font-style: normal;
		    width: 200px;
		    border: none;
		    border-width: 1px 1px 3px;
		    margin-bottom: 10px;
		    height: 50px;
		}
		.form-style-5 input[type="submit"]:hover,
		.form-style-5 input[type="button"]:hover
		{
		    background: #eeca05;
		}
	

		</style>
	</head>
<body>
<div class="header">

	<div class="container">
		<img src="img/qualy.svg"/>		
		<ul class="nav">		
		<li><a href="table.php" class="btn-mini">View</a></li>
		<li><a href="logout.php" class="btn-mini">Log Out</a></li>
		</ul>
	</div>
</div>
<div class="content">
	<div class="form-style-5">
		<form action="insert-row.php" method="post">
			<fieldset>
			<legend><img src="img/qualy.svg"/><span class="number">Add new record</span></legend>
				<input type="number" name="proposalNumber" placeholder="Proposal Number *" required>
				<input type="text" name="customerName" placeholder="Customer Name *" required>
				<input type="date" name="orderDate" placeholder="Order Date *" required>
				<input type="date" name="requestDeliveryTime" placeholder="Request Delivery Time*" required>
				<input type="date" name="deliveryDate" placeholder="Delivery Date *" >
				<input type="url" name="testTargetURL" placeholder="Test Target URL *" required>      
				<input type="text" name="remark" placeholder="Remark *" >      
			</fieldset>
				<input type="submit" value="Apply" />
		</form>
	</div>
</div>
</body>
</html>