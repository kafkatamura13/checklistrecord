<?php
	require 'confs/auth.php';
	require 'confs/config.php';
	session_start();

	$sql="SELECT * FROM usertb";
	$result=mysql_query($sql);

 ?>
	<!DOCTYPE html>
	<html>
	<head>
		<title></title>
   		 <meta charset="utf-8">
   		 <link rel="icon" type="image/png" href="img/767175.png">
		<!-- <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/reset.css"/> -->
		<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
		<style type="text/css">
					table th,td{
			text-align: center;
		}
		th , td {
			font-size: 12px;
			padding: 0px;
			margin:0px;
		}
	/*	.hide {
		display: none;
		}*/
		tbody tr:hover {
		cursor: default;
		
	}
	tbody tr a:hover {
		cursor:pointer;
	}
	.remark {
		color:#f00;
	}
/*	footer {
		height: 100px;
	}*/
	footer .container {
		padding-top: 20px;

	}
	footer .container p{
		text-align: center;
	}
	.content {
		height: 500px;
	}
	label {
		display: block;
	}
	</style>

	</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="table.php">QA-TPL のプロジェっト</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
   

      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['user']; ?><span class="caret"></span></a>
          <ul class="dropdown-menu" >
            <li><a href="administration.php">Admin Panel</a></li>            
            <li><a href="logout.php">Log Out</a></li>    
          
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


<div class="content">
<div class="container">
	<div class="row">	
		<div class="col-md-6">
	<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading">Admin Console</div>
	

  <!-- List group -->
	<div class="list-group">
	  <button type="button" class="list-group-item">Cras justo odio</button>
	  <button type="button" class="list-group-item">Dapibus ac facilisis in</button>
	  <button type="button" class="list-group-item">Morbi leo risus</button>
	  <button type="button" class="list-group-item">Porta ac consectetur ac</button>
	  <button type="button" class="list-group-item">Vestibulum at eros</button>
	</div>
	</div>
</div>
		<div class="col-md-6 left-pane">
		<div class="panel panel-primary">		
		<div class="panel-heading panel-primary">Users</div>
		<table class="table table-condensed table-hover table-responsive">
			<thead>
				<th>Sr.</th>
				<th>Username</th>
				<th>User Role</th>
				<th colspan="2">Option</th>
			</thead>
			<tbody>
			<?php while($row=mysql_fetch_assoc($result)): ?>
				<tr>
				<td><?php echo $row['u_id'] ?></td>
				<td><?php echo $row['u_name'] ?></td>
				<td><?php echo $row['u_role'] ?></td>			
				<td><a href="edit_user.php?id=<?php echo $row['u_id'] ?>" title="Edit Description"><span class="glyphicon glyphicon-pencil"></span></a></td>
				<td><a href="javascript:delete_id(<?php echo $row['u_id']; ?>)" title="Delete Row"><span class="glyphicon glyphicon-trash"></span></a></td>			
				</tr>
			<?php endwhile; ?>
			</tbody>
			
		</table>
		
		</div>
		<button type="button" class="btn btn-primary btn-xs" onclick="addUser()">Add New User</button>
	</div>

	</div>
	<div class="row">
	<div class="col-md-6 left-pane"></div>
	<div class="col-md-6 left-pane">

	</div>
	</div>
</div>
</div>

<script src="bower_components/jquery/dist/jquery.js"></script>
<script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
		function delete_id(id){
		if(confirm('Sure To Remove This Record ' + id + '?')){
        window.location.href='delete_user.php?id='+id;


     }
	}
</script>

</body>
</html>
